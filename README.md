## Editor Prereqs:
* [ESLint](http://eslint.org/docs/user-guide/integrations)
* [EditorConfig](http://editorconfig.org/#download)

## Getting this running
* `npm install`
* Copy `.env.example` to `.env` and adjust any settings
* `gulp run` OR `gulp dev`.
 * `gulp dev` will restart the server on code changes and give you access to the inspector

## API Docs
* `gulp apidoc`
* `http://{{your host}}/docs`