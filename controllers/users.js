'use strict;';

const db = require('../models');

const ctrl = {
  auth(req, res) {
    db.users.findOneByEmail(req.body.email)
      .then((user) => {
        if (!user) return Promise.reject('no user found');
        return user.checkPassword(req.body.password)
          .then(() => user);
      })
      .then(user => user.generateJWT())
      .then(user => res.json(user))
      .catch((e) => {
        if (e.name === 'NoUser') {
          res.status(422).json({ validationErrors: ['No User Found'] });
        } else if (e.name === 'MismatchError' && e.message === 'invalid') {
          res.status(422).json({ validationErrors: ['Invalid Password'] });
        } else {
          console.error(e);
          res.status(500).json(e);
        }
      });
  },
  decodeJWT(req, res, next, jwt) {
    jwt({ secret: process.env.JWT_SECRET }, () => next());
  },
  register(req, res) {
    const userParams = {
      email: req.body.email,
      password: req.body.password,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      permissions: ['user'],
    };

    db.users.create(userParams)
      .then(user => user.generateJWT())
      .then((user) => {
        res.json(user);
      })
      .catch((e) => {
        if (e.name === 'SequelizeUniqueConstraintError') {
          res.status(422).json({
            type: 'dupUser',
            msg: 'Email address already registered',
          });
        } else {
          console.error(e);
          res.status(500).json(e);
        }
      });
  },
  userInfo(req, res) {
    db.users.findOneById(req.user.id)
      .then(user => res.json(user));
  },
};

module.exports = ctrl;
