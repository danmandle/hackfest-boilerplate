const gulp = require('gulp');
const apidoc = require('gulp-apidoc');
const eslint = require('gulp-eslint');
const nodeInspector = require('gulp-node-inspector');
const nodemon = require('gulp-nodemon');
// const notify = require('gulp-notify');

gulp.task('run', () => {
  nodemon({
    script: 'server.js',
    ext: 'js',
    ignore: ['public/**/*', 'gulpfile.js', 'node_modules/**/*'],
    nodeArgs: [],
  });
});

gulp.task('nodemon-debug', () => {
  nodemon({
    script: 'server.js',
    ext: 'js pug env yml',
    nodeArgs: ['--debug'],
  });
});

gulp.task('dev', ['nodemon-debug'], () => {
  gulp.src([])
    .pipe(nodeInspector({
      preload: true,
      saveLiveEdit: true,
      // debugPort: 5858,
      // webPort: 8080,
      // stackTraceLimit: 50,
    }));
});

gulp.task('lint', () => {
  // ESLint ignores files with "node_modules" paths.
  // So, it's best to have gulp ignore the directory as well.
  // Also, Be sure to return the stream from the task;
  // Otherwise, the task may end before the stream has finished.
  return gulp.src(['**/*.js', '!node_modules/**'])
    // eslint() attaches the lint output to the "eslint" property
    // of the file object so it can be used by other modules.
    .pipe(eslint())
    // eslint.format() outputs the lint results to the console.
    // Alternatively use eslint.formatEach() (see Docs).
    .pipe(eslint.format())
    // To have the process exit with an error code (1) on
    // lint error, return the stream and pipe to failAfterError last.
    .pipe(eslint.failAfterError());
});

gulp.task('docs', (done) => {
  apidoc({
    src: 'routes/',
    dest: 'public/docs/',
  }, done);
});
