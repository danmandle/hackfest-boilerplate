
docker run -dt \
-p 5432:5432 \
-e POSTGRES_USER=postgres \
-e POSTGRES_PASSWORD=hackfest \
-e POSTGRES_DB=hackfest \
--name hackfest-postgis \
--restart="unless-stopped" \
mdillon/postgis