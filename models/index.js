'use strict;';

const Sequelize = require('sequelize');

const postgres = {
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DATABASE,
};

const sequelize = new Sequelize(postgres.database, postgres.user, postgres.password, {
  host: postgres.host,
  port: postgres.port,
  dialect: 'postgres',
  logging: process.env.SEQUELIZE_LOGGING !== 'false', // so that even if you have a type-o, it'll default to true
});

console.log(`Connecting to Postgres DB ${postgres.database} on ${postgres.host}:${postgres.port}`);

const models = {};

const incModels = [
  'users',
];

incModels.forEach((incModel) => {
  models[incModel] = sequelize.import(`./${incModel}.js`);
  models[incModel].sync()
    .then(() => {
      console.log(`done syncing ${incModel} model`);
    });
});

module.exports = models;
