'use strict;';

const bcrypt = require('bcrypt-as-promised');
const jwt = require('jsonwebtoken');

function encryptPassword(user) {
  if (!user.changed('password')) return Promise.resolve();

  return bcrypt.genSalt(10)
    .then(salt => bcrypt.hash(user.getDataValue('password'), salt))
    .then(hashedPassword => user.setDataValue('password', hashedPassword))
    .catch((e) => {
      console.error('error while setting password', e);
      return Promise.reject(e);
    });
}

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true,
      },
    },
    password: {
      type: DataTypes.STRING,
    },
    firstName: {
      type: DataTypes.STRING,
    },
    lastName: {
      type: DataTypes.STRING,
    },
    permissions: {
      type: DataTypes.JSONB,
    },
    name: {
      type: DataTypes.VIRTUAL,
      get() {
        return `${this.getDataValue('firstName')} ${this.getDataValue('lastName')}`;
      },
    },
    jwt: {
      type: DataTypes.VIRTUAL,
    },
  }, {
    classMethods: {
      findOneByEmail(email) {
        return this.findOne({ where: { email } });
      },
      findOneById(id) {
        return this.findOne({ where: { id } });
      },
    },
    instanceMethods: {
      checkPassword(givenPassword) {
        return bcrypt.compare(givenPassword, this.getDataValue('password'))
          .then(() => this);
      },
      generateJWT() {
        const payload = {
          id: this.getDataValue('id'),
          email: this.getDataValue('email'),
          firstName: this.getDataValue('firstName'),
          lastName: this.getDataValue('lastName'),
          name: this.name,
          permissions: ['user'],
        };

        const signedJWT = jwt.sign(payload, process.env.JWT_SECRET, {
          expiresIn: '2 days',
        });

        this.setDataValue('jwt', signedJWT);

        return Promise.resolve(this);
      },
      toJSON() {
        const values = Object.assign({}, this.get());

        delete values.password;
        return values;
      },
    },
    hooks: {
      beforeCreate: encryptPassword,
      beforeUpdate: encryptPassword,
    },
    indexes: [
      {
        unique: true,
        fields: ['email'],
      },
    ],
  });
};
