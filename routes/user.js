'use strict;';

const express = require('express');
const guard = require('express-jwt-permissions')();
const jwt = require('express-jwt');

const router = express.Router();

router.use(jwt({ secret: process.env.JWT_SECRET }).unless({
  path: [
    '/user/auth',
    '/user/register',
  ],
}));

router.get('/', require('../controllers/users.js').userInfo);
/**
 * @api {get} /user/ User Info
 * @apiName getUser
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Value must be `Bearer`, followed by a space, followed by the JWT
 * @apiHeaderExample {json} Header-Example:
 *   {
 *     "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiZW1haWwiOiJ5ZXNAbm8uY29tIiwiZmlyc3ROYW1lIjoiWWVzIiwibGFzdE5hbWUiOiJObyIsIm5hbWUiOiJZZXMgTm8iLCJwZXJtaXNzaW9ucyI6WyJ1c2VyIl0sImlhdCI6MTQ4ODE1ODY1OSwiZXhwIjoxNDg4MzMxNDU5fQ.SRnMxSf5ahU8IPrTT_GALPJH-X2PCalUI5arn5Sc2O8"
 *   }
 *
 * @apiSuccess {Number} id  The id of the user.
 * @apiSuccess {String} firstName Firstname of the User.
 * @apiSuccess {String} lastName  Lastname of the User.
 * @apiSuccess {Array} permissions  An array of strings that represent what permissions the user has.
 * @apiSuccess {String} name  Concatination of `firstName` and `lastName`.
 * @apiSuccess {String} email  Email address of user.
 * @apiSuccess {String} createdAt  When the record was created.
 * @apiSuccess {String} updatedAt  When the record was last updated.
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *    "id": 7,
 *    "email": "morty@mortysmith.com",
 *    "firstName": "Morty",
 *    "lastName": "Smith",
 *    "name": "Morty Smith",
 *    "permissions": ['user', 'admin'],
 *    "createdAt": "2017-02-27T00:41:12.000Z",
 *    "updatedAt": "2017-02-27T00:41:12.000Z"
 *  }
 */

router.post('/auth', require('../controllers/users.js').auth);
/**
 * @api {post} /user/auth Auth/Login
 * @apiName auth
 * @apiGroup User
 *
 * @apiParam {String} email User's registered email address
 * @apiParam {String} password User's password
 *
 * @apiSuccess {Number} id  The id of the user.
 * @apiSuccess {String} firstName Firstname of the User.
 * @apiSuccess {String} lastName  Lastname of the User.
 * @apiSuccess {Array} permissions  An array of strings that represent what permissions the user has.
 * @apiSuccess {String} name  Concatination of `firstName` and `lastName`.
 * @apiSuccess {String} email  Email address of user.
 * @apiSuccess {String} jwt  JSON Web Token
 * @apiSuccess {String} createdAt  When the record was created.
 * @apiSuccess {String} updatedAt  When the record was last updated.
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *    "id": 7,
 *    "email": "morty@mortysmith.com",
 *    "firstName": "Morty",
 *    "lastName": "Smith",
 *    "name": "Morty Smith",
 *    "permissions": ['user', 'admin'],
 *    "jwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiZW1haWwiOiJ5ZXNAbm8uY29tIiwiZmlyc3ROYW1lIjoiWWVzIiwibGFzdE5hbWUiOiJObyIsIm5hbWUiOiJZZXMgTm8iLCJwZXJtaXNzaW9ucyI6WyJ1c2VyIl0sImlhdCI6MTQ4ODE2ODQ4MiwiZXhwIjoxNDg4MzQxMjgyfQ.PLgKXmavTzSA-bqcLvh7c71-4_6vA_PcSMJMxWkEcb0",
 *    "createdAt": "2017-02-27T00:41:12.000Z",
 *    "updatedAt": "2017-02-27T00:41:12.000Z"
 *  }
 *
 * @apiError (Error 422) {String[]} validationErrors Array of human readable strings
 *
 */

router.get('/jwt', (req, res) => res.json(req.user));
/**
 * @api {get} /user/jwt JWT
 * @apiName jwt
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Value must be `Bearer`, followed by a space, followed by the JWT
 * @apiHeaderExample {json} Header-Example:
 *   {
 *     "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiZW1haWwiOiJ5ZXNAbm8uY29tIiwiZmlyc3ROYW1lIjoiWWVzIiwibGFzdE5hbWUiOiJObyIsIm5hbWUiOiJZZXMgTm8iLCJwZXJtaXNzaW9ucyI6WyJ1c2VyIl0sImlhdCI6MTQ4ODE1ODY1OSwiZXhwIjoxNDg4MzMxNDU5fQ.SRnMxSf5ahU8IPrTT_GALPJH-X2PCalUI5arn5Sc2O8"
 *   }
 *
 * @apiSuccess {Object} id  The id of the user.
 * @apiSuccess {String} firstName Firstname of the User.
 * @apiSuccess {String} lastName  Lastname of the User.
 * @apiSuccess {String[]} permissions  An array of strings that represent what permissions the user has.
 * @apiSuccess {String} name  Concatination of `firstName` and `lastName`.
 * @apiSuccess {String} email  Email address of user.
 * @apiSuccess {String} createdAt  When the record was created.
 * @apiSuccess {String} updatedAt  When the record was last updated.
 * @apiSuccess {Number} iat  Issued at time in seconds since EPOCH
 * @apiSuccess {Number} exp  Expiration time in seconds since EPOCH
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *    "id": 7,
 *    "email": "morty@mortysmith.com",
 *    "firstName": "Morty",
 *    "lastName": "Smith",
 *    "name": "Morty Smith",
 *    "permissions": ['user', 'admin'],
 *    "createdAt": "2017-02-27T00:41:12.000Z",
 *    "updatedAt": "2017-02-27T00:41:12.000Z",
 *    "iat": 1488168482,
 *    "exp": 1488341282
 *  }
 *
 *
 */

router.post('/register', require('../controllers/users.js').register);
/**
 * @api {get} /user/register Register
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Value must be `Bearer`, followed by a space, followed by the JWT
 * @apiHeaderExample {json} Header-Example:
 *   {
 *     "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiZW1haWwiOiJ5ZXNAbm8uY29tIiwiZmlyc3ROYW1lIjoiWWVzIiwibGFzdE5hbWUiOiJObyIsIm5hbWUiOiJZZXMgTm8iLCJwZXJtaXNzaW9ucyI6WyJ1c2VyIl0sImlhdCI6MTQ4ODE1ODY1OSwiZXhwIjoxNDg4MzMxNDU5fQ.SRnMxSf5ahU8IPrTT_GALPJH-X2PCalUI5arn5Sc2O8"
 *   }
 *
 * @apiSuccess {Number} id  The id of the user.
 * @apiSuccess {String} firstName Firstname of the User.
 * @apiSuccess {String} lastName  Lastname of the User.
 * @apiSuccess {Array} permissions  An array of strings that represent what permissions the user has.
 * @apiSuccess {String} name  Concatination of `firstName` and `lastName`.
 * @apiSuccess {String} email  Email address of user.
 * @apiSuccess {String} createdAt  When the record was created.
 * @apiSuccess {String} updatedAt  When the record was last updated.
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *    "id": 7,
 *    "email": "morty@mortysmith.com",
 *    "firstName": "Morty",
 *    "lastName": "Smith",
 *    "name": "Morty Smith",
 *    "permissions": ['user', 'admin'],
 *    "createdAt": "2017-02-27T00:41:12.000Z",
 *    "updatedAt": "2017-02-27T00:41:12.000Z"
 *  }
 *
 * @apiError (Error 422) {String} type Type of error
 * @apiError (Error 422) {String} msg Human readable error message
 *
 */

// router.get('/protected', guard.check(['admin', 'user']), (req, res) => res.json(req.user));

module.exports = router;
