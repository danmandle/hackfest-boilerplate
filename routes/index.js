const router = require('express').Router();
const fsp = require('fs-promise');

// Grab all of the files in the routes folder
fsp.readdir(__dirname)
  .then(files => {
    files.map(file => {
      if(file.charAt(0) !== '.' && file !== 'index.js' && file !== 'apidoc.json'){
        let routeName = file.split('.')[0]; // strip the extension

        if (routeName == 'root') {
          // attach the root routes at the root
          router.use('/', require('./root'));
        } else {
          // attach the route at the name of the route file
          router.use(`/${routeName}`, require(`./${routeName}`));
        }
      }
    });
  });

module.exports = router;
